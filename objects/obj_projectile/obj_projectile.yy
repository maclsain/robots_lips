{
    "id": "ee11c4fe-3a37-420e-abe2-b5588f931a70",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_projectile",
    "eventList": [
        {
            "id": "12ed1377-b839-4448-947e-d033ad54da30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ee11c4fe-3a37-420e-abe2-b5588f931a70"
        },
        {
            "id": "685e82b4-d43f-4cb2-a341-065a7c38ef13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "ee11c4fe-3a37-420e-abe2-b5588f931a70"
        },
        {
            "id": "c57e0e6c-6abe-43f2-9543-e1a7ac0d1c9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ee11c4fe-3a37-420e-abe2-b5588f931a70"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8b18d80d-cc19-42b8-87bc-66f2646b04e4",
    "visible": true
}