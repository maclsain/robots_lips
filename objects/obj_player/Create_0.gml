///obj_player variables

//constants
playerGravityMax = 4.0;
playerGravityFactor = 1.2;
playerGravityDir = 270; //Down. 0 = right, number is in degrees going counter clockwise
playerMoveSpeed = 0.8;
PlayerMoveSpeedMax = 11.0;
playerJumpFactor = 24.0;
isOnGround = 0;
h_placemeeting = false;
v_placemeeting = false;

//movement
isMoving = false;
h_momentum = 0.0;
v_momentum = 0.0;
playerDir = 1;
canFire = 1;
alarm[0] = 10;