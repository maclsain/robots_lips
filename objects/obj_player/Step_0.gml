//keyboard controls
key_jump_1 = keyboard_check_pressed(vk_space);
key_jump_2 = keyboard_check_pressed(vk_up);

key_left = keyboard_check(vk_left);
key_right = keyboard_check(vk_right);

key_shoot_1 = keyboard_check(vk_lcontrol);
key_shoot_2 = keyboard_check(vk_rcontrol);

key_melee = keyboard_check(vk_lalt);
key_melee = keyboard_check(vk_ralt);

//-------------------------------------------------------------------------
//horizontal collision
if(place_meeting(x+h_momentum, y-10, obj_block)){
	while(!place_meeting(x+sign(h_momentum), y-10, obj_block)){
		x += sign(h_momentum);
	}
	h_momentum = 0; //This breaks horizontal movement
}

//Horizontal bounds check
if((x+h_momentum) < 0 || (x+h_momentum) > room_height){
	h_momentum = 0;
}

x += h_momentum;

//-------------------------------------------------------------------------
//vertical collision
if(place_meeting(x, y+v_momentum, obj_block)){
	while(!place_meeting(x, y+sign(v_momentum), obj_block)){
		y += sign(v_momentum);
		isOnGround = false;
	}

	if(v_momentum >= playerGravityMax){
		v_momentum = playerGravityMax;
	}
	v_momentum = 0;
}		
else{
	v_momentum += playerGravityFactor;

}

//Hacky way to stop infinite jumping.
if(v_momentum == 0){
	isOnGround = true;
}
else{
	isOnGround = false;
}

y += v_momentum;

//--------------------------------------------------------------------------
//movement code
if(key_right && !key_left){
	if(h_momentum < PlayerMoveSpeedMax){
		h_momentum += playerMoveSpeed * 1.2;
		playerDir = 1;
	}
}
else if(key_left && !key_right){
	if(h_momentum > -PlayerMoveSpeedMax){
		h_momentum -= playerMoveSpeed * 1.2;
		playerDir = -1;
	}
}
else if(!key_left && !key_right){
	if(h_momentum > 0){
		h_momentum -= playerMoveSpeed;
	}
	if(h_momentum < 0){
		h_momentum += playerMoveSpeed;
	}
	if(h_momentum > 0 && h_momentum < 1.0){
		h_momentum = 0;
	}
}

//Jumping 
if((key_jump_1 || key_jump_2) && isOnGround){
	v_momentum = -playerJumpFactor;
	audio_play_sound(Jump3, 90, false);
}

//-------------------------------------------------------------------------
//Animations

image_xscale = playerDir;


//Shooting
if(key_shoot_1 && canFire == 1){
	instance_create_depth(x, y, 0, obj_projectile);
	audio_play_sound(kiss3, 90, false);
	canFire = 0;
	alarm[0] = 10;
}

