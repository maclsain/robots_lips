{
    "id": "d19c52eb-cd1a-4280-ac19-0d1b1164f62d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "b28ee053-7f6d-4414-b649-56755a81484f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d19c52eb-cd1a-4280-ac19-0d1b1164f62d"
        },
        {
            "id": "46ea3ba1-8e4f-44cc-873e-bf450f8803bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d19c52eb-cd1a-4280-ac19-0d1b1164f62d"
        },
        {
            "id": "9d23fa0b-a981-4adc-b718-a1e982bf7729",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d19c52eb-cd1a-4280-ac19-0d1b1164f62d"
        },
        {
            "id": "08b6d452-1382-40f8-b073-3dda92e47a41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d19c52eb-cd1a-4280-ac19-0d1b1164f62d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "14bed788-a596-4ecc-af1f-a47a9c52c004",
    "visible": true
}