///Settings
audio_channel_num(16);
thisRoom = room_get_name(room);

//Stupid way to track what background should be up on closing_screen

//creating and setting variables
var thisRoomHeight = room_height;
var thisRoomWidth = room_height;

//set game to be sized as per room size
var screenWidth = thisRoomHeight;
var screenHeight = thisRoomWidth;

window_set_size(screenWidth, screenHeight);

//how much of the room is visible in the view
view_wview = screenWidth;
view_hview = screenHeight;

view_wport = screenWidth;
view_hport = screenHeight;

surface_resize(application_surface, screenWidth, screenHeight);


//Game logic
global.numEnemies = instance_number(obj_movEnemy) + instance_number(obj_jetEnemy) + instance_number(obj_remo) + instance_number(obj_breckon) + instance_number(obj_rodkin);