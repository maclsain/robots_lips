{
    "id": "4d65bacc-878f-466f-9c22-53163fc4ae1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game",
    "eventList": [
        {
            "id": "2e7b17da-43e8-42c0-b092-a8d0265272a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d65bacc-878f-466f-9c22-53163fc4ae1a"
        },
        {
            "id": "0e30e2b1-a894-44d5-bfb1-98e040b93b9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d65bacc-878f-466f-9c22-53163fc4ae1a"
        },
        {
            "id": "f3082e19-fca9-452f-b79d-6f175515957d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 10,
            "m_owner": "4d65bacc-878f-466f-9c22-53163fc4ae1a"
        },
        {
            "id": "b875e74d-8600-4256-878e-a9f367487660",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4d65bacc-878f-466f-9c22-53163fc4ae1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}