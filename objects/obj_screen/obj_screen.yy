{
    "id": "2f372cdd-ba71-4553-8eb1-82675ed5e862",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_screen",
    "eventList": [
        {
            "id": "27755553-f8fc-4865-b6ee-940132c474ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f372cdd-ba71-4553-8eb1-82675ed5e862"
        },
        {
            "id": "a3f34fde-e235-465f-ba7b-fa0efd950eed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "2f372cdd-ba71-4553-8eb1-82675ed5e862"
        },
        {
            "id": "d8cfeb26-442f-495f-94f9-7d89d5ac4cdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2f372cdd-ba71-4553-8eb1-82675ed5e862"
        },
        {
            "id": "deb511f1-3ed8-4f06-9247-b5bfe4c09321",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2f372cdd-ba71-4553-8eb1-82675ed5e862"
        },
        {
            "id": "d7701353-de52-4229-b617-d4fedcdb95a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2f372cdd-ba71-4553-8eb1-82675ed5e862"
        },
        {
            "id": "b2093d21-f96c-46f9-b287-ca31b54712e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2f372cdd-ba71-4553-8eb1-82675ed5e862"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}