if(!audio_is_playing(music_love_theme)){
	audio_stop_all();
	audio_play_sound(music_love_theme, 90, true);
}

roomLayers = layer_get_all();

startFrequency = 60;

/*
for(var i = 0; i == array_length_1d(roomLayers); i++){
	layer_set_visible(i,false);
}*/

if(room == 0){
	global.closingScreenIndex = 0;
	layer_set_visible("Background",true);
	alarm_set(0, startFrequency);
}
else
if(room != 0){

	switch global.closingScreenIndex{
		case 0:
		if(layer_exists("bg_lastLevel")){
			layer_set_visible("bg_lastLevel", true);
		}
		break;
		case 1:
		if(layer_exists("bg_endScreen")){
			layer_set_visible("bg_endScreen", true);
		}
		break;
		case 2:
		if(layer_exists("bg_credits")){
			layer_set_visible("bg_credits", true);
		}
		break;
	}
}