{
    "id": "ca371849-ee5e-490d-a6d0-072b8036bad3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_breckon",
    "eventList": [
        {
            "id": "561a4ce8-3e68-49ac-bc59-de9cf8ec435d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca371849-ee5e-490d-a6d0-072b8036bad3"
        },
        {
            "id": "e281339e-703e-4f5e-b8b7-db2898902e67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca371849-ee5e-490d-a6d0-072b8036bad3"
        },
        {
            "id": "2f07924b-33d2-43ea-bb8f-602b36135c6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ca371849-ee5e-490d-a6d0-072b8036bad3"
        },
        {
            "id": "4cc4629f-0494-4991-b034-58ec721d3108",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee11c4fe-3a37-420e-abe2-b5588f931a70",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ca371849-ee5e-490d-a6d0-072b8036bad3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "117339b2-2d9e-406e-a4ac-c5aac1fe6246",
    "visible": true
}