if(isActive = true){
	moveSpeed = 0;
	viewRange = 0;
	instance_create_depth(x, y - 64, -64, obj_speech);
	with (obj_projectile){
		if(place_meeting(x,y,obj_movEnemy)){
			audio_play_sound(kissed2, 90, false);
			instance_destroy();
		}
	}
	global.numEnemies -= 1;
	isActive = false;
}