{
    "id": "ffdf2966-b70d-4fb3-8712-8709d57bf1bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_movEnemy",
    "eventList": [
        {
            "id": "e6ba06d3-bd82-4885-b684-4dd2a37249b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ffdf2966-b70d-4fb3-8712-8709d57bf1bf"
        },
        {
            "id": "c87e03f4-29da-4da6-ad2b-d20dafcba71e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ffdf2966-b70d-4fb3-8712-8709d57bf1bf"
        },
        {
            "id": "703b56ee-0a1b-4885-9bfe-2c05f5793a28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ffdf2966-b70d-4fb3-8712-8709d57bf1bf"
        },
        {
            "id": "05c1b707-1e91-451a-a4a5-591ed30dde37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee11c4fe-3a37-420e-abe2-b5588f931a70",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ffdf2966-b70d-4fb3-8712-8709d57bf1bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "20b88fc6-888a-4a02-9d09-55ac22180ad6",
    "visible": true
}