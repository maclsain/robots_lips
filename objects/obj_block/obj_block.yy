{
    "id": "5a37322d-1823-4de2-bcc3-d33df85cb95c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_block",
    "eventList": [
        {
            "id": "d53ca634-95cd-4dd8-b749-20fb8327b104",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee11c4fe-3a37-420e-abe2-b5588f931a70",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5a37322d-1823-4de2-bcc3-d33df85cb95c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "0794f775-3c9e-4d0c-b92f-1411a82b71da",
    "visible": true
}