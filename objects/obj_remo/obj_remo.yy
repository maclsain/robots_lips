{
    "id": "79ed55a7-1ed0-45ab-a706-2096dd606869",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_remo",
    "eventList": [
        {
            "id": "7d873978-877b-443e-abd0-d36cb3d52859",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79ed55a7-1ed0-45ab-a706-2096dd606869"
        },
        {
            "id": "92e3df7b-9e1a-4673-8cec-1142084c135b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "79ed55a7-1ed0-45ab-a706-2096dd606869"
        },
        {
            "id": "9e32857e-dfc6-4a86-bf4c-7b05c41e39cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "79ed55a7-1ed0-45ab-a706-2096dd606869"
        },
        {
            "id": "35bb0102-1d37-4c14-9d7a-94e0ee464b8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee11c4fe-3a37-420e-abe2-b5588f931a70",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "79ed55a7-1ed0-45ab-a706-2096dd606869"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "03475bc8-f3c0-4ee3-afe3-f6e242565391",
    "visible": true
}