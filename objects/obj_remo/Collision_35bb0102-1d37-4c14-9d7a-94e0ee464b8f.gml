if(isActive = true){
	moveSpeed = 0;
	viewRange = 0;
	image_index = 1;
	with (obj_projectile){
		if(place_meeting(x,y,obj_remo)){
			audio_play_sound(kissed2, 90, false);
			instance_destroy();
		}
	}
	global.numEnemies -= 1;
	isActive = false;
}