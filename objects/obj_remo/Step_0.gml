var distanceToPlayer = point_distance(x, y, obj_player.x, obj_player.y);
var directionToPlayer = point_direction(x, 0, obj_player.x, 0);

if(distanceToPlayer <= viewRange){
    motion_set(-directionToPlayer,-moveSpeed);
    stopMoovingDelay = 1;
}
else if (stopMoovingDelay == 1){
    alarm[0] = 30;
    stopMoovingDelay = 0;
}

//-------------------------------------------------------------------------
//horizontal collision
if(place_meeting(x+h_momentum, y, obj_block)){
    while(!place_meeting(x+sign(h_momentum), y, obj_block)){
        x += sign(h_momentum);
    }
    //h_momentum = 0; This breaks horizontal movement
}

if((x-moveSpeed) < 0 || (x-moveSpeed) > room_width){
	moveSpeed = 0;
}

x += h_momentum;
//-------------------------------------------------------------------------
//vertical collision
if(place_meeting(x, y+v_momentum, obj_block)){
    while(!place_meeting(x, y+sign(v_momentum), obj_block)){
        y += sign(v_momentum);
        isOnGround = false;
    }

    if(v_momentum >= enemyGravityMax){
        v_momentum = enemyGravityMax;
    }
    v_momentum = 0;
}        
else{
    v_momentum += enemyGravityFactor;
    isOnGround = true;
}

y += v_momentum;