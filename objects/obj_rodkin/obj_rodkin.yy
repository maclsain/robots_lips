{
    "id": "623e8edc-402c-4aa0-983c-de1bfe44ccec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rodkin",
    "eventList": [
        {
            "id": "24c782ff-9860-41c7-b0df-3fe455cbe466",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "623e8edc-402c-4aa0-983c-de1bfe44ccec"
        },
        {
            "id": "495d6d3e-467f-4cb2-bbc5-1666cf77e77c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "623e8edc-402c-4aa0-983c-de1bfe44ccec"
        },
        {
            "id": "6db7fea9-00fb-44a4-9940-697e0b6434ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "623e8edc-402c-4aa0-983c-de1bfe44ccec"
        },
        {
            "id": "f4f52f9a-9a2b-4270-ac9b-79ed352bf04f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee11c4fe-3a37-420e-abe2-b5588f931a70",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "623e8edc-402c-4aa0-983c-de1bfe44ccec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "fdc91f6d-d75a-40db-a85a-398a5536a3bc",
    "visible": true
}