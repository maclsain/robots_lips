moveSpeed = 3;
moveDistance = 6;
enemyGravityDir = 270; //Down. 0 = right, number is in degrees going counter clockwise
h_momentum = 0.0;
v_momentum = 0.0;
stopMoovingDelay = 0;
isActive = true;
yOrigin = y;
returnPath = false;

//convert move distance to pixels
yFinal = yOrigin + (moveDistance * 32);