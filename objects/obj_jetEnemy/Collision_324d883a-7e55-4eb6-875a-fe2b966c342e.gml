if(isActive = true){
	moveSpeed = 0;
	moveDistance = 0;
	with (obj_projectile){
		if(place_meeting(x,y,obj_jetEnemy)){
			audio_play_sound(kissed2, 90, false);
			instance_destroy();
		}
	}
	global.numEnemies -= 1;
	isActive = false;
	alarm_set(1, 1);
}