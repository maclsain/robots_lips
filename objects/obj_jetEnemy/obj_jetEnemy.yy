{
    "id": "1ed1e0ed-b024-44ba-a391-c755f535eb90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_jetEnemy",
    "eventList": [
        {
            "id": "20862577-d5cc-46a4-a62f-bf976b257460",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ed1e0ed-b024-44ba-a391-c755f535eb90"
        },
        {
            "id": "324d883a-7e55-4eb6-875a-fe2b966c342e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee11c4fe-3a37-420e-abe2-b5588f931a70",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1ed1e0ed-b024-44ba-a391-c755f535eb90"
        },
        {
            "id": "8bbdf13c-79cf-4c4f-8de1-5aed0a1600d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1ed1e0ed-b024-44ba-a391-c755f535eb90"
        },
        {
            "id": "4eebb45b-95a1-4dfc-89cd-a7a299271804",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "1ed1e0ed-b024-44ba-a391-c755f535eb90"
        },
        {
            "id": "0271ee0e-6f71-4318-bca8-eba99364d4de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1ed1e0ed-b024-44ba-a391-c755f535eb90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b25e16a1-e66a-4b76-bce4-e983a9a289b7",
    "visible": true
}