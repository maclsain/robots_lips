{
    "id": "8b18d80d-cc19-42b8-87bc-66f2646b04e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_projectile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7d85d74e-7fcc-482a-bcaf-2673701cb88f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b18d80d-cc19-42b8-87bc-66f2646b04e4",
            "compositeImage": {
                "id": "0fba9fbc-3c03-44eb-98af-37075d5fcc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d85d74e-7fcc-482a-bcaf-2673701cb88f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86c2e01-e1d1-4d29-b472-5c3e2bd95130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d85d74e-7fcc-482a-bcaf-2673701cb88f",
                    "LayerId": "15e20798-7fe3-481b-a7b6-2b1afc3a271a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "15e20798-7fe3-481b-a7b6-2b1afc3a271a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b18d80d-cc19-42b8-87bc-66f2646b04e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}