{
    "id": "0645354d-e6f4-49ea-a354-ed7458be88e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_endScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d2e06382-1615-42b5-81b3-8ed32e68e924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0645354d-e6f4-49ea-a354-ed7458be88e1",
            "compositeImage": {
                "id": "371d7739-ceef-4d4c-9661-871dd1cbc8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e06382-1615-42b5-81b3-8ed32e68e924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b951e37-e973-4e2e-8637-5a250f0d24d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e06382-1615-42b5-81b3-8ed32e68e924",
                    "LayerId": "cd911029-89bc-4766-b332-d23ebb209dd9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "cd911029-89bc-4766-b332-d23ebb209dd9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0645354d-e6f4-49ea-a354-ed7458be88e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}