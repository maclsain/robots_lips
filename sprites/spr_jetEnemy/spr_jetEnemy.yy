{
    "id": "b25e16a1-e66a-4b76-bce4-e983a9a289b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jetEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 123,
    "bbox_left": 21,
    "bbox_right": 126,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "efae5eb7-8eae-42fd-b781-09aa73ce7dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25e16a1-e66a-4b76-bce4-e983a9a289b7",
            "compositeImage": {
                "id": "5b324126-e372-4198-929d-e0f162371a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efae5eb7-8eae-42fd-b781-09aa73ce7dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0cc940-86a0-4f8b-8029-ba72c5b416dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efae5eb7-8eae-42fd-b781-09aa73ce7dd5",
                    "LayerId": "5b3074a4-38f6-46ff-a248-bb629c3e3ec3"
                }
            ]
        },
        {
            "id": "707d1eee-4e07-414d-9f4e-caf8151720b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25e16a1-e66a-4b76-bce4-e983a9a289b7",
            "compositeImage": {
                "id": "85db81d7-7913-4878-abe9-3053eaa1bd0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "707d1eee-4e07-414d-9f4e-caf8151720b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961b9409-8569-42d7-b61d-7b96cca34018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "707d1eee-4e07-414d-9f4e-caf8151720b7",
                    "LayerId": "5b3074a4-38f6-46ff-a248-bb629c3e3ec3"
                }
            ]
        },
        {
            "id": "a1a33626-2d69-40e3-b679-0b612aabca7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25e16a1-e66a-4b76-bce4-e983a9a289b7",
            "compositeImage": {
                "id": "b7a8ecc9-4ab9-46e5-b001-682f6273b0bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1a33626-2d69-40e3-b679-0b612aabca7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13811b1a-a43a-4c58-9423-09056a01ea51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1a33626-2d69-40e3-b679-0b612aabca7b",
                    "LayerId": "5b3074a4-38f6-46ff-a248-bb629c3e3ec3"
                }
            ]
        },
        {
            "id": "3d78838f-c811-4342-9242-df81f53c8f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b25e16a1-e66a-4b76-bce4-e983a9a289b7",
            "compositeImage": {
                "id": "cfd50f06-777a-4ed7-9a2f-3410b7ebf6d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d78838f-c811-4342-9242-df81f53c8f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c5a908-91fe-4596-b757-94731bcc2a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d78838f-c811-4342-9242-df81f53c8f12",
                    "LayerId": "5b3074a4-38f6-46ff-a248-bb629c3e3ec3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5b3074a4-38f6-46ff-a248-bb629c3e3ec3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b25e16a1-e66a-4b76-bce4-e983a9a289b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}