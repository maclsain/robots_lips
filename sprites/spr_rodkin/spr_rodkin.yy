{
    "id": "fdc91f6d-d75a-40db-a85a-398a5536a3bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rodkin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 22,
    "bbox_right": 104,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a3788b82-093c-42f8-af5e-1096fe69bde8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc91f6d-d75a-40db-a85a-398a5536a3bc",
            "compositeImage": {
                "id": "c26c3153-77d7-4859-a8f2-38ad45861c81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3788b82-093c-42f8-af5e-1096fe69bde8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "524900f4-eff0-4cb4-a66b-540894316c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3788b82-093c-42f8-af5e-1096fe69bde8",
                    "LayerId": "a2c087f7-aa2b-4348-b7ba-5778005f0b70"
                }
            ]
        },
        {
            "id": "13c4cb6a-4eaf-46da-bfd1-d0f10a315a3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc91f6d-d75a-40db-a85a-398a5536a3bc",
            "compositeImage": {
                "id": "0cbf10ce-d6e0-4275-be95-c2ada738fc50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c4cb6a-4eaf-46da-bfd1-d0f10a315a3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb5a211-b6e4-4115-b199-6362f2ab8870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c4cb6a-4eaf-46da-bfd1-d0f10a315a3d",
                    "LayerId": "a2c087f7-aa2b-4348-b7ba-5778005f0b70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a2c087f7-aa2b-4348-b7ba-5778005f0b70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdc91f6d-d75a-40db-a85a-398a5536a3bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}