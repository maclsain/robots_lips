{
    "id": "0794f775-3c9e-4d0c-b92f-1411a82b71da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1b1b8695-04c2-40a9-afc9-cfea7315daaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0794f775-3c9e-4d0c-b92f-1411a82b71da",
            "compositeImage": {
                "id": "f04a8860-62da-4c35-9ce3-ecd7edefd0c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b1b8695-04c2-40a9-afc9-cfea7315daaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a888106-76f8-43bf-92d4-777689494027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b1b8695-04c2-40a9-afc9-cfea7315daaf",
                    "LayerId": "9d435983-7c81-42f4-9c2a-5f61ffae5391"
                }
            ]
        },
        {
            "id": "073af4f6-7c50-45a7-bd6d-aa7c30653093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0794f775-3c9e-4d0c-b92f-1411a82b71da",
            "compositeImage": {
                "id": "cba17823-6f38-43d1-84f6-1233f634cefa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "073af4f6-7c50-45a7-bd6d-aa7c30653093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "025277e2-aa29-4566-958d-aeb40297996e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "073af4f6-7c50-45a7-bd6d-aa7c30653093",
                    "LayerId": "9d435983-7c81-42f4-9c2a-5f61ffae5391"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d435983-7c81-42f4-9c2a-5f61ffae5391",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0794f775-3c9e-4d0c-b92f-1411a82b71da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}