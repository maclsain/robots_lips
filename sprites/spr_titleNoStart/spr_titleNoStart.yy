{
    "id": "ee9731c2-1d5e-4bcb-8110-697a13e6582b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titleNoStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e340e444-1a67-4de7-8511-e9665a322e55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9731c2-1d5e-4bcb-8110-697a13e6582b",
            "compositeImage": {
                "id": "00561cb2-03c4-4fa5-bdc4-c5a18b79420d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e340e444-1a67-4de7-8511-e9665a322e55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "420b02fc-fb90-4171-a0b2-ff282bc4b426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e340e444-1a67-4de7-8511-e9665a322e55",
                    "LayerId": "2ec31901-148b-4330-b31f-84cc727b2e2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "2ec31901-148b-4330-b31f-84cc727b2e2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee9731c2-1d5e-4bcb-8110-697a13e6582b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}