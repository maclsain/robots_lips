{
    "id": "117339b2-2d9e-406e-a4ac-c5aac1fe6246",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_breckon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 22,
    "bbox_right": 104,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c3a1a244-4050-46eb-aff1-7314aa1acaa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117339b2-2d9e-406e-a4ac-c5aac1fe6246",
            "compositeImage": {
                "id": "12d81a72-42b5-4f0c-84f2-1207cd96427b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a1a244-4050-46eb-aff1-7314aa1acaa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a3db3c-5330-40e9-8563-e899cd8418ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a1a244-4050-46eb-aff1-7314aa1acaa8",
                    "LayerId": "728d0a86-7a00-415b-9f22-19ae6d769b34"
                }
            ]
        },
        {
            "id": "d0803c3f-0a66-4f0c-b787-b27b73334be5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117339b2-2d9e-406e-a4ac-c5aac1fe6246",
            "compositeImage": {
                "id": "7ef7ed22-562d-41af-97e1-4d4375fba808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0803c3f-0a66-4f0c-b787-b27b73334be5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38c0f73-c8a6-489d-8407-9ae63771142c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0803c3f-0a66-4f0c-b787-b27b73334be5",
                    "LayerId": "728d0a86-7a00-415b-9f22-19ae6d769b34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "728d0a86-7a00-415b-9f22-19ae6d769b34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "117339b2-2d9e-406e-a4ac-c5aac1fe6246",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}