{
    "id": "b3120a8d-2899-4d73-9057-0eab37d95525",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_thmbsScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6bc250f8-0f6e-490e-88a6-8591080d5b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3120a8d-2899-4d73-9057-0eab37d95525",
            "compositeImage": {
                "id": "8d8db4a5-3408-4acd-95fe-2bf8fa1731ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc250f8-0f6e-490e-88a6-8591080d5b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56676651-0c0c-446e-91e5-7c75eee35529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc250f8-0f6e-490e-88a6-8591080d5b1c",
                    "LayerId": "1d225c71-3bf7-4b96-b7e7-fdf147a1afa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "1d225c71-3bf7-4b96-b7e7-fdf147a1afa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3120a8d-2899-4d73-9057-0eab37d95525",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}