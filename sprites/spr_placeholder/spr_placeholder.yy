{
    "id": "c4a8066a-d11f-4d82-ae98-2c5a33345ca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_placeholder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3152b562-c802-4d60-9283-8beef2a35039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4a8066a-d11f-4d82-ae98-2c5a33345ca3",
            "compositeImage": {
                "id": "d8edc21f-7cad-4c42-8c93-2bcd3edeb236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3152b562-c802-4d60-9283-8beef2a35039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81d498fa-6895-4133-b1f8-a58066e88ac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3152b562-c802-4d60-9283-8beef2a35039",
                    "LayerId": "e0ce2c28-457b-4bae-b721-385df23dac7a"
                },
                {
                    "id": "c721a736-c969-4db3-a62e-c61353add3b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3152b562-c802-4d60-9283-8beef2a35039",
                    "LayerId": "ccd4c005-6ece-4362-9303-efe8f9047672"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "ccd4c005-6ece-4362-9303-efe8f9047672",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4a8066a-d11f-4d82-ae98-2c5a33345ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e0ce2c28-457b-4bae-b721-385df23dac7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4a8066a-d11f-4d82-ae98-2c5a33345ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}