{
    "id": "722b1e8e-37dd-4cb8-b6c2-5ccaff27162b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_creditsScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bf6238b9-f175-4a78-ac67-fe5b31a76dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "722b1e8e-37dd-4cb8-b6c2-5ccaff27162b",
            "compositeImage": {
                "id": "81a03b62-b1e9-432b-9fe7-1aa7f36fa6c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6238b9-f175-4a78-ac67-fe5b31a76dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "163a29a8-423a-4891-975c-410823cb0946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6238b9-f175-4a78-ac67-fe5b31a76dbb",
                    "LayerId": "15ca3bd5-8014-46e1-b157-b629405ffcae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "15ca3bd5-8014-46e1-b157-b629405ffcae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "722b1e8e-37dd-4cb8-b6c2-5ccaff27162b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}