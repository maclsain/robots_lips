{
    "id": "af0d94d6-a1e9-41b1-8d44-679ea6f0cfe8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7a993e61-4f7e-42a3-bd6e-5dacbc826dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af0d94d6-a1e9-41b1-8d44-679ea6f0cfe8",
            "compositeImage": {
                "id": "8ded5b4e-6d59-4cca-9578-7c20407dca4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a993e61-4f7e-42a3-bd6e-5dacbc826dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "152de9ca-08f8-4f1b-8ef5-4d638d5c6d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a993e61-4f7e-42a3-bd6e-5dacbc826dde",
                    "LayerId": "830f411c-2c98-410b-895e-7a5af4a4660e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "830f411c-2c98-410b-895e-7a5af4a4660e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af0d94d6-a1e9-41b1-8d44-679ea6f0cfe8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}