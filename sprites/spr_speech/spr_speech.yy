{
    "id": "00cf557b-468d-4565-be05-56ec26c30b39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_speech",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 15,
    "bbox_right": 109,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a2da0c21-9e7b-4457-9d5e-d1b0f903f91f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00cf557b-468d-4565-be05-56ec26c30b39",
            "compositeImage": {
                "id": "c389af4e-2f10-41d8-bc67-cdf65db5471f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2da0c21-9e7b-4457-9d5e-d1b0f903f91f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4e91b4-48c2-432d-9769-e296374b068e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2da0c21-9e7b-4457-9d5e-d1b0f903f91f",
                    "LayerId": "3f79e656-e4ed-43e4-8201-dfc538d91f05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3f79e656-e4ed-43e4-8201-dfc538d91f05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00cf557b-468d-4565-be05-56ec26c30b39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 127
}