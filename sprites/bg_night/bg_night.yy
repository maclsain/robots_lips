{
    "id": "8eed0492-c117-4482-8ecc-fce085aa9e6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_night",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0417a89e-e8ec-4a2a-b67b-5c7ae02456ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eed0492-c117-4482-8ecc-fce085aa9e6d",
            "compositeImage": {
                "id": "f3d543f1-0c20-4c86-a24c-75856c90ddc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0417a89e-e8ec-4a2a-b67b-5c7ae02456ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c8e1e1-1cac-4ed4-9a3f-74f48939dcec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0417a89e-e8ec-4a2a-b67b-5c7ae02456ee",
                    "LayerId": "5f1ab509-3cfa-4396-bb93-9fd06d2d8ff0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "5f1ab509-3cfa-4396-bb93-9fd06d2d8ff0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8eed0492-c117-4482-8ecc-fce085aa9e6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}