{
    "id": "a2e36dcb-848a-4c4b-b2f1-73a44d6f645d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_idle1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 449,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8fbd1dfc-255a-4852-9d39-db9a1030dab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2e36dcb-848a-4c4b-b2f1-73a44d6f645d",
            "compositeImage": {
                "id": "4d96590f-245f-44b6-a69b-9dd34fddf25f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fbd1dfc-255a-4852-9d39-db9a1030dab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f09c2531-bf0c-4e13-a7b4-c34410c89fdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fbd1dfc-255a-4852-9d39-db9a1030dab8",
                    "LayerId": "c956ef0e-e4d1-4c5d-9792-51a81dfd380d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 450,
    "layers": [
        {
            "id": "c956ef0e-e4d1-4c5d-9792-51a81dfd380d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2e36dcb-848a-4c4b-b2f1-73a44d6f645d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}