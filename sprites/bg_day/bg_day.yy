{
    "id": "9a124388-9e93-4fae-9fbc-77c11c3ce7f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_day",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "30985f0f-0576-42ba-82d6-71dfbaa5a968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a124388-9e93-4fae-9fbc-77c11c3ce7f8",
            "compositeImage": {
                "id": "38e283a4-a647-46c9-acf4-3f8a177b837b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30985f0f-0576-42ba-82d6-71dfbaa5a968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6958b6bc-e41c-452a-bdbb-ed67e412373d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30985f0f-0576-42ba-82d6-71dfbaa5a968",
                    "LayerId": "a44aadb7-64a0-4753-8a05-77148678959b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "a44aadb7-64a0-4753-8a05-77148678959b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a124388-9e93-4fae-9fbc-77c11c3ce7f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}