{
    "id": "03475bc8-f3c0-4ee3-afe3-f6e242565391",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_remo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 22,
    "bbox_right": 104,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "af4b7cfb-187e-475f-9809-9351efa191fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03475bc8-f3c0-4ee3-afe3-f6e242565391",
            "compositeImage": {
                "id": "c4644278-bbcb-47c6-8924-d9f92e556516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4b7cfb-187e-475f-9809-9351efa191fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb68988a-5d8c-4eff-9dfb-87734bf923b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4b7cfb-187e-475f-9809-9351efa191fb",
                    "LayerId": "a483132f-9f42-4419-a8c2-4122c745d7d6"
                }
            ]
        },
        {
            "id": "6f48ba89-282a-4b79-8eae-79e0073ae653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03475bc8-f3c0-4ee3-afe3-f6e242565391",
            "compositeImage": {
                "id": "245e774a-4ba0-49e9-8725-957a01242321",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f48ba89-282a-4b79-8eae-79e0073ae653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11723296-fa48-477a-9aee-5a22fcbc72ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f48ba89-282a-4b79-8eae-79e0073ae653",
                    "LayerId": "a483132f-9f42-4419-a8c2-4122c745d7d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a483132f-9f42-4419-a8c2-4122c745d7d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03475bc8-f3c0-4ee3-afe3-f6e242565391",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}