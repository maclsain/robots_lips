{
    "id": "20b88fc6-888a-4a02-9d09-55ac22180ad6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_movEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 28,
    "bbox_right": 99,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d3bd060b-5cc3-43a2-a3a2-6b4085a97562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b88fc6-888a-4a02-9d09-55ac22180ad6",
            "compositeImage": {
                "id": "d5867097-16db-4016-842e-e3984fb16d02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3bd060b-5cc3-43a2-a3a2-6b4085a97562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb07d3b1-486b-4c30-a8d5-2b0d18d1193b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3bd060b-5cc3-43a2-a3a2-6b4085a97562",
                    "LayerId": "9efbb3c2-af90-4925-9aae-865a7068bae5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9efbb3c2-af90-4925-9aae-865a7068bae5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20b88fc6-888a-4a02-9d09-55ac22180ad6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}