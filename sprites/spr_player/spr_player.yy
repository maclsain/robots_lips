{
    "id": "14bed788-a596-4ecc-af1f-a47a9c52c004",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 26,
    "bbox_right": 105,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8616cdbf-c37c-4513-b35c-b3d1db3b5895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14bed788-a596-4ecc-af1f-a47a9c52c004",
            "compositeImage": {
                "id": "6914a3e9-e2e8-4fb2-9840-0c258c098539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8616cdbf-c37c-4513-b35c-b3d1db3b5895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10519fac-5553-4bd0-a013-10fb71f40ae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8616cdbf-c37c-4513-b35c-b3d1db3b5895",
                    "LayerId": "67d69a84-9392-4461-81cd-8d712b37c8a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "67d69a84-9392-4461-81cd-8d712b37c8a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14bed788-a596-4ecc-af1f-a47a9c52c004",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}