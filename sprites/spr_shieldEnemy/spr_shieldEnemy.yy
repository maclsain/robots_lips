{
    "id": "326dfa09-0d49-4580-a43c-8fd3f7b38008",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shieldEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "99a810e2-7826-4a32-89d9-3444909dd703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "326dfa09-0d49-4580-a43c-8fd3f7b38008",
            "compositeImage": {
                "id": "c9fd8ae1-d6f6-4ad2-9984-f3cf816e36aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99a810e2-7826-4a32-89d9-3444909dd703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70cd9a63-a937-465e-ab15-458e4a87ee8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99a810e2-7826-4a32-89d9-3444909dd703",
                    "LayerId": "488727cd-58d1-4f6e-add8-1af725394c0f"
                }
            ]
        },
        {
            "id": "880594fb-dce2-4562-a65c-c9d87de77e41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "326dfa09-0d49-4580-a43c-8fd3f7b38008",
            "compositeImage": {
                "id": "2b645bdc-e9c4-478c-a5cf-5c17b1f8394c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880594fb-dce2-4562-a65c-c9d87de77e41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a216fc31-2436-4c7e-9a9d-c722b3331e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880594fb-dce2-4562-a65c-c9d87de77e41",
                    "LayerId": "488727cd-58d1-4f6e-add8-1af725394c0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "488727cd-58d1-4f6e-add8-1af725394c0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "326dfa09-0d49-4580-a43c-8fd3f7b38008",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}