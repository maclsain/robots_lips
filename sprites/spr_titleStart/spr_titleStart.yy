{
    "id": "c9d924d2-8fd8-4bde-8874-18975262b050",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titleStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 329,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "84442117-21a6-48ea-ad6b-ae2be506eceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9d924d2-8fd8-4bde-8874-18975262b050",
            "compositeImage": {
                "id": "9faa5f41-a9e7-48cb-976c-0991a53f4173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84442117-21a6-48ea-ad6b-ae2be506eceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "980035b8-7373-4530-914b-e2349318db63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84442117-21a6-48ea-ad6b-ae2be506eceb",
                    "LayerId": "8b50afe3-9f48-450d-a5b9-10c5253419b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "8b50afe3-9f48-450d-a5b9-10c5253419b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9d924d2-8fd8-4bde-8874-18975262b050",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 331,
    "xorig": 165,
    "yorig": 14
}